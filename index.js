// header
const log = console.log;
const app = new PIXI.Application();
document.body.appendChild(app.view);



// consts
const MAX_BIZZ = 6;
const AMARELO_TOTALBIZZ = 3;
const VERDE_TOTALBIZZ = 3;
const BONUS_VOLTACOMPLETA = 2000;



// classes
class Player {
    constructor(id, name) {
        this.id = id;
        this.name = name;

        this.money = 2000;
        this.pos = 0;
  	    this.lastdice1 = 0;
  	    this.lastdice2 = 0;
    };
}
class Bizz {
    constructor(id, name, price, cor) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.cor = cor;

  	    this.housesPrice = price * 2;
  	    this.rent = (price / 5) + (price * (this.houses + 1));
    		this.houses = 0;
    		this.owner = 0;
    };
}



// functions
const RollDice = () => {
	var value = Math.floor(Math.random() * 6 + 1);
	return value;
}
const Walk = (player, qnt) => {
	log("a andar: " + qnt + " minha pos: " + player.pos);
	var towalk = player.pos + qnt;
	if(towalk > MAX_BIZZ) { // andar mais que o limite do tabuleiro
		var resto = towalk - MAX_BIZZ;
		player.pos = resto;
		// deu uma volta
		player.money += BONUS_VOLTACOMPLETA;
		log('DEU UMA VOLTA');
	}
	else { // andar normal, menor que o limite
		player.pos += qnt;
	}
}
const BuyBizz = (player, bizz) => {
	var price = bizz.price;
	if(player.money < price) {
		return alert('Você não tem dinheiro suficiente. (você tem: ' + this.money + ', precisa de: ' + price);
	}
	player.money -= money;
	bizz.owner = player.id;
}
const MakeHouse = (player, bizz) => {
	var price = bizz.housesPrice;
	if(player.money < price) {
		return alert('Você não tem dinheiro suficiente. (você tem: ' + this.money + ', precisa de: ' + price);
	}
	player.money -= money;
	bizz.houses += 1;
}
const Play = (player) => {
	var dice1 = RollDice();
	var dice2 = RollDice();
	var dados = dice1 + dice2;
	log("dado 1: " + dice1 + " && dado 2: " + dice2 + " = " + dados);

	Walk(player, dados);

	var whereim = player.pos;
	log(bizz[whereim].name);
}
const SorteReves = (player) => {
	var randChance = Math.floor(Math.random() * 100 + 1);
	var randValue = Math.floor(Math.random() * 50000 + 2000);
	if(rand >= 1 && rand <= 50) {
		log("REVES: você perdeu: " + randValue);
		player.money -= randValue;
	}
	else if(rand >= 51 && rand <= 100) {
		log("Sorte: você ganhou: " + randValue);
		player.money += randValue;
	}
}



// change background
app.renderer.backgroundColor = 0x061639;
// full screen
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);



// docs
var player = [];
player[1] = new Player(1, 'Tiago');
player[2] = new Player(2, 'Luiz');


var bizz = [];
bizz[1] = new Bizz(1, 'Nova America', 10000, 'amarelo');
bizz[2] = new Bizz(2, 'Catedral', 10000, 'amarelo');
bizz[3] = new Bizz(3, 'Norte Shopping', 10000, 'amarelo');
bizz[4] = new Bizz(4, 'Itarare', 10000, 'verde');
bizz[5] = new Bizz(5, 'Grota', 10000, 'verde');
bizz[6] = new Bizz(6, 'Coca Cola', 10000, 'verde');

bizz[7] = new Bizz(7, 'Test7', 10000, 'vermelho');
bizz[8] = new Bizz(8, 'Test8', 10000, 'vermelho');
bizz[9] = new Bizz(9, 'Test9', 10000, 'vermelho');
bizz[10] = new Bizz(10, 'Test10', 10000, 'vermelho');
bizz[11] = new Bizz(11, 'Test11', 10000, 'vermelho');
bizz[12] = new Bizz(12, 'Test12', 10000, 'vermelho');
bizz[13] = new Bizz(13, 'Test13', 10000, 'vermelho');
bizz[14] = new Bizz(14, 'Test14', 10000, 'vermelho');
bizz[15] = new Bizz(15, 'Test15', 10000, 'vermelho');
bizz[16] = new Bizz(16, 'Test16', 10000, 'vermelho');
bizz[17] = new Bizz(17, 'Test17', 10000, 'vermelho');
bizz[18] = new Bizz(18, 'Test18', 10000, 'vermelho');
bizz[19] = new Bizz(19, 'Test19', 10000, 'vermelho');
bizz[20] = new Bizz(20, 'Test20', 10000, 'vermelho');



// actions
Play(player[1]);
Play(player[1]);
Play(player[1]);
