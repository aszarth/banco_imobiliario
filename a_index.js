// sound 1
// const Howl = require('howler');
// import {Howl, Howler} from 'howler';

const log = console.log;
const app = new PIXI.Application();
document.body.appendChild(app.view);

const CreateItem = (image, mp3, ogg, callback, x=0, y=0, rotation=0, scale=1) =>
{
	const item = PIXI.Sprite.fromImage(image);
	/* sound 3
	const sound = new Hool({
		src: [mp3, ogg]
	});
	*/
	item.anchor.set(0.5);
	item.interactive = true;
	item.buttonMode = true;
	item.x = x;
	item.y = y;
	item.rotation = rotation;
	item.scale.set(scale);
	app.stage.addChild(item);

	const onDragStart = event => {
		item.data = event.data;
		item.dragging = true;
	};
	const onDragEnd = event => {
		delete item.data;
		item.dragging = false;
	};
	const onDragMove = event => {
		if(item.dragging === true)
		{
			const newPosition = item.data.getLocalPosition(item.parent);
			item.x = newPosition.x;
			item.y = newPosition.y;
		}
	};
	
	item.on('pointerdown', onDragStart)
	.on('pointerup', onDragEnd)
	.on('pointerupoutside', onDragEnd)
	.on('pointermove', onDragMove);

	app.ticker.add((delta) => {
		if(item.dragging === true)
		{
			item.rotation += 0.1 * delta;
		}
	});

	item.on('pointerdown', () => {
		log('plim');
		/* sound 2
		var sound = new Howl({
		  src: ['./sounds/faca.mp3']
		});
		sound.play();
		*/
	});
	return item;
}

// change background
app.renderer.backgroundColor = 0x061639;
// full screen
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);


var item = CreateItem('./imgs/Sword 6.png', './sounds/mp3/faca.mp3', './sounds/ogg/faca.ogg', 100, 100, 0, 1);
var sword = CreateItem('./imgs/Axe 8.png', './sounds/mp3/faca.mp3', './sounds/ogg/faca.ogg', 300, 300, 0, 1);
var armor = CreateItem('./imgs/Armor 4.png', './sounds/mp3/faca.mp3', './sounds/ogg/faca.ogg', 500, 500, 0, 1);